package com.algaworks.algafood;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import org.hamcrest.Matchers;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.algaworks.algafood.api.controller.RestauranteController;
import com.algaworks.algafood.domain.model.Cozinha;
import com.algaworks.algafood.domain.model.FormaPagamento;
import com.algaworks.algafood.domain.model.Restaurante;
import com.algaworks.algafood.domain.repository.CozinhaRepository;
import com.algaworks.algafood.domain.repository.FormaPagamentoRepository;
import com.algaworks.algafood.domain.repository.RestauranteRepository;
import com.algaworks.algafood.utils.test.DatabaseCleaner;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = { "spring.config.location=classpath:application-test.yml" })
public class CadastroRestauranteIT {

	@LocalServerPort
	private int port;
	
	@Value("${server.servlet.context-path}")
	private String basePath;
	
	@Autowired
	private DatabaseCleaner databaseCleaner;
	
	@Autowired
	private CozinhaRepository cozinhaRepository;
	
	@Autowired
	private FormaPagamentoRepository formaPagamentoRepository;
	
	@Autowired
	private RestauranteRepository restauranteRepository;
	
	private FormaPagamento formaPagamentoCartaoCredito;
	
	private FormaPagamento formaPagamentoCartaoDebito;
	
	private FormaPagamento formaPagamentoPix;
	
	private Cozinha cozinhaJaponesa;
	
	private Cozinha cozinhaChinesa;
	
	private Cozinha cozinhaBrasileira;
	
	private Restaurante restauranteYamato;
	
	private Restaurante restauranteMiLig;
	
	private Restaurante restauranteFogoDeChao;
	
	private Restaurante novoRestauranteParaCadastro;

	private Long qtdRestaurantesCadastrados;
	
	private static final Long RESTAURANTE_ID_INEXISTENTE = 945665L;
	
	@Before
	public void setup() {
		RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
		RestAssured.basePath = basePath;
		RestAssured.port = port;
		
		this.preparaMassaDeDados();
	}
	
	@After
	public void cleanDatabase() {
		databaseCleaner.clearTables();
	}
	
	private void preparaMassaDeDados() {
		FormaPagamento fp1 = new FormaPagamento();
		fp1.setDescricao("Cartão de Crédito");
		formaPagamentoCartaoCredito = formaPagamentoRepository.save(fp1);
		
		FormaPagamento fp2 = new FormaPagamento();
		fp2.setDescricao("Cartão de Débito");
		formaPagamentoCartaoDebito = formaPagamentoRepository.save(fp2);
		
		FormaPagamento fp3 = new FormaPagamento();
		fp3.setDescricao("Pix");
		formaPagamentoPix = formaPagamentoRepository.save(fp3);
		
		/*-----------------------------------------------*/
		
		Cozinha c1 = new Cozinha();
		c1.setNome("Japonesa");
		cozinhaJaponesa = cozinhaRepository.save(c1);
		
		Cozinha c2 = new Cozinha();
		c2.setNome("Chinesa");
		cozinhaChinesa = cozinhaRepository.save(c2);
		
		Cozinha c3 = new Cozinha();
		c3.setNome("Brasileira");
		cozinhaBrasileira = cozinhaRepository.save(c3);
		
		/*-----------------------------------------------*/
		
		Restaurante restaurante1 = new Restaurante();
		restaurante1.setNome("Yamato");
		restaurante1.setTaxaFrete(new BigDecimal(10.5));
		restaurante1.setCozinha(cozinhaJaponesa);
		List<FormaPagamento> listaFormaPagamentosRestaurante1 = new LinkedList<>();
		listaFormaPagamentosRestaurante1.add(formaPagamentoCartaoCredito);
		listaFormaPagamentosRestaurante1.add(formaPagamentoCartaoDebito);
		listaFormaPagamentosRestaurante1.add(formaPagamentoPix);
		restaurante1.setFormasPagamento(listaFormaPagamentosRestaurante1);
		restauranteYamato = restauranteRepository.save(restaurante1);
		
		Restaurante restaurante2 = new Restaurante();
		restaurante2.setNome("Mi Lig");
		restaurante2.setTaxaFrete(new BigDecimal(4));
		restaurante2.setCozinha(cozinhaChinesa);
		List<FormaPagamento> listaFormaPagamentosRestaurante2 = new LinkedList<>();
		listaFormaPagamentosRestaurante2.add(formaPagamentoPix);
		restaurante2.setFormasPagamento(listaFormaPagamentosRestaurante2);
		restauranteMiLig = restauranteRepository.save(restaurante2);
		
		Restaurante restaurante3 = new Restaurante();
		restaurante3.setNome("Fogo de Chão");
		restaurante3.setTaxaFrete(new BigDecimal(7));
		restaurante3.setCozinha(cozinhaBrasileira);
		List<FormaPagamento> listaFormaPagamentosRestaurante3 = new LinkedList<>();
		listaFormaPagamentosRestaurante3.add(formaPagamentoPix);
		restaurante3.setFormasPagamento(listaFormaPagamentosRestaurante3);
		restauranteFogoDeChao = restauranteRepository.save(restaurante3);
		
		qtdRestaurantesCadastrados = restauranteRepository.count();
		
		/*-----------------------------------------------*/
		
		Restaurante novoRestaurante = new Restaurante();
		novoRestaurante.setNome("Restaurante do amanhã");
		novoRestaurante.setTaxaFrete(new BigDecimal(8));
		novoRestaurante.setCozinha(cozinhaJaponesa);
		List<FormaPagamento> listaFormaPagamentos = new LinkedList<>();
		listaFormaPagamentos.add(formaPagamentoCartaoDebito);
		listaFormaPagamentos.add(formaPagamentoPix);
		novoRestaurante.setFormasPagamento(listaFormaPagamentos);
		
		novoRestauranteParaCadastro = novoRestaurante;
	}
	
	@Test
	public void deveRetornar200_QuandoConsultarRestaurantes() {
		RestAssured.given()
			.accept(ContentType.JSON)
		.when()
			.get(RestauranteController.ENDPOINT_URI)
		.then()
			.statusCode(HttpStatus.OK.value());
	}
	
	@Test
	public void deveRetornar3Restaurantes_QuandoConsultarRestaurantes() {
		RestAssured.given()
			.accept(ContentType.JSON)
		.when()
			.get(RestauranteController.ENDPOINT_URI)
		.then()
			.body("", Matchers.hasSize(qtdRestaurantesCadastrados.intValue()))
			.body("nome", Matchers.hasItems(restauranteYamato.getNome(), restauranteMiLig.getNome(),
					restauranteFogoDeChao.getNome()));
	}
	
	@Test
	public void deveRetornar201_QuandoCadastrarRestauranteValido() {
		JSONObject json = new JSONObject(novoRestauranteParaCadastro);
		
		RestAssured.given()
			.body(json.toString())
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
		.when()
			.post(RestauranteController.ENDPOINT_URI)
		.then()
			.statusCode(HttpStatus.CREATED.value());
	}
	
	@Test
	public void deveRetornar400_QuandoCadastrarRestauranteTaxaZeradoESemDescricaoFreteGratis() {
		novoRestauranteParaCadastro.setTaxaFrete(new BigDecimal(0));
		JSONObject json = new JSONObject(novoRestauranteParaCadastro);
		
		RestAssured.given()
			.body(json.toString())
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
		.when()
			.post(RestauranteController.ENDPOINT_URI)
		.then()
			.statusCode(HttpStatus.BAD_REQUEST.value());
	}
	
	@Test
	public void deveRetornar400_QuandoCadastrarRestauranteTaxaZeradoEComDescricaoFreteGratis() {
		novoRestauranteParaCadastro.setNome("Restaurante do amanhã - Frete Grátis");
		novoRestauranteParaCadastro.setTaxaFrete(new BigDecimal(0));
		JSONObject json = new JSONObject(novoRestauranteParaCadastro);
		
		RestAssured.given()
			.body(json.toString())
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
		.when()
			.post(RestauranteController.ENDPOINT_URI)
		.then()
			.statusCode(HttpStatus.CREATED.value());
	}
	
	@Test
	public void deveRetornar400_QuandoCadastrarRestauranteSemTaxaFrete() {
		novoRestauranteParaCadastro.setTaxaFrete(null);
		JSONObject json = new JSONObject(novoRestauranteParaCadastro);
		
		RestAssured.given()
			.body(json.toString())
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
		.when()
			.post(RestauranteController.ENDPOINT_URI)
		.then()
			.statusCode(HttpStatus.BAD_REQUEST.value());
	}
	
	@Test
	public void deveRetornar400_QuandoCadastrarRestauranteSemNome() {
		novoRestauranteParaCadastro.setNome(null);
		JSONObject json = new JSONObject(novoRestauranteParaCadastro);
		
		RestAssured.given()
			.body(json.toString())
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
		.when()
			.post(RestauranteController.ENDPOINT_URI)
		.then()
			.statusCode(HttpStatus.BAD_REQUEST.value());
	}
	
	@Test
	public void deveRetornar400_QuandoCadastrarRestauranteSemListaFormasPagamento() {
		novoRestauranteParaCadastro.setFormasPagamento(null);
		JSONObject json = new JSONObject(novoRestauranteParaCadastro);
		
		RestAssured.given()
			.body(json.toString())
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
		.when()
			.post(RestauranteController.ENDPOINT_URI)
		.then()
			.statusCode(HttpStatus.BAD_REQUEST.value());
	}
	
	@Test
	public void deveRetornar400_QuandoCadastrarRestauranteSemCozinha() {
		novoRestauranteParaCadastro.setCozinha(null);		
		JSONObject json = new JSONObject(novoRestauranteParaCadastro);
		
		RestAssured.given()
			.body(json.toString())
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
		.when()
			.post(RestauranteController.ENDPOINT_URI)
		.then()
			.statusCode(HttpStatus.BAD_REQUEST.value());
	}
	
	@Test
	public void deveRetornar200ERestaurante_QuandoConsultarUmRestaurante() {
		String restauranteId = "restauranteId";
		
		RestAssured.given()
			.pathParam(restauranteId, restauranteMiLig.getId())
			.accept(ContentType.JSON)
		.when()
			.get(RestauranteController.ENDPOINT_URI+"/{"+restauranteId+"}")
		.then()
			.statusCode(HttpStatus.OK.value())
			.body("nome", Matchers.equalTo(restauranteMiLig.getNome()));
	}
	
	@Test
	public void deveRetornar404_QuandoConsultarUmRestauranteInexistente() {
		String restauranteId = "restauranteId";
		
		RestAssured.given()
			.pathParam(restauranteId, RESTAURANTE_ID_INEXISTENTE)
			.accept(ContentType.JSON)
		.when()
			.get(RestauranteController.ENDPOINT_URI+"/{"+restauranteId+"}")
		.then()
			.statusCode(HttpStatus.NOT_FOUND.value());
	}
	
}
