package com.algaworks.algafood;

import org.hamcrest.Matchers;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.algaworks.algafood.api.controller.CozinhaController;
import com.algaworks.algafood.domain.model.Cozinha;
import com.algaworks.algafood.domain.repository.CozinhaRepository;
import com.algaworks.algafood.utils.test.DatabaseCleaner;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = { "spring.config.location=classpath:application-test.yml" })
public class CadastroCozinhaIT {
	
	@LocalServerPort
	private int port;
	
	@Value("${server.servlet.context-path}")
	private String basePath;
	
	@Autowired
	private DatabaseCleaner databaseCleaner;
	
	@Autowired
	private CozinhaRepository cozinhaRepository;
	
	private Long qtdCozinhas;
	
	private Cozinha cozinhaJaponesa;
	
	private Cozinha cozinhaChinesa;
	
	private Cozinha cozinhaBrasileira;
	
	private static final Long COZINHA_ID_INEXISTENTE = 154983L;
	
	@Before
	public void setup() {
		RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
		RestAssured.basePath = basePath;
		RestAssured.port = port;
		
		this.preparaMassaDeDados();
	}
	
	@After
	public void cleanDatabase() {
		databaseCleaner.clearTables();
	}
	
	private void preparaMassaDeDados() {
		Cozinha c1 = new Cozinha();
		c1.setNome("Japonesa");
		cozinhaJaponesa = cozinhaRepository.save(c1);
		
		Cozinha c2 = new Cozinha();
		c2.setNome("Chinesa");
		cozinhaChinesa = cozinhaRepository.save(c2);
		
		Cozinha c3 = new Cozinha();
		c3.setNome("Brasileira");
		cozinhaBrasileira = cozinhaRepository.save(c3);
		
		qtdCozinhas = cozinhaRepository.count();
	}
	
	@Test
	public void deveRetornarStatus200_QuandoConsultarCozinhas() {
		RestAssured.given()
			.accept(ContentType.JSON)
		.when()
			.get(CozinhaController.ENDPOINT_URI)
		.then()
			.statusCode(HttpStatus.OK.value());
	}
	
	@Test
	public void deveConter4Cozinhas_QuandoConsultarCozinhas() {		
		RestAssured.given()
			.accept(ContentType.JSON)
		.when()
			.get(CozinhaController.ENDPOINT_URI)
		.then()
			.body("", Matchers.hasSize(qtdCozinhas.intValue()))
			.body("nome", Matchers.hasItems(cozinhaJaponesa.getNome(), cozinhaChinesa.getNome(),
					cozinhaBrasileira.getNome()));
	}
	
	@Test
	public void deveRetornarStatus201_QunadoCadastrarCozinha() {
		Cozinha cozinha = new Cozinha();
		cozinha.setNome("Francesa");
		JSONObject json = new JSONObject(cozinha);		
		
		RestAssured.given()
			.body(json.toString())
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
		.when()
			.post(CozinhaController.ENDPOINT_URI)
		.then()
			.statusCode(HttpStatus.CREATED.value());
	}
	
	@Test
	public void deveRetornar200ECozinha_QuandoConsultarCozinhaExistente() {
		String cozinhaId = "cozinhaId";
		
		RestAssured.given()
			.pathParam(cozinhaId, cozinhaChinesa.getId())
			.accept(ContentType.JSON)
		.when()
			.get(CozinhaController.ENDPOINT_URI+"/{"+cozinhaId+"}")
		.then()
			.statusCode(HttpStatus.OK.value())
			.body("nome", Matchers.equalTo(cozinhaChinesa.getNome()));
	}
	
	@Test
	public void deveRetornar404_QuandoConsultarCozinhaInexistente() {
		String cozinhaId = "cozinhaId";
		
		RestAssured.given()
			.pathParam(cozinhaId, COZINHA_ID_INEXISTENTE)
			.accept(ContentType.JSON)
		.when()
			.get(CozinhaController.ENDPOINT_URI+"/{"+cozinhaId+"}")
		.then()
			.statusCode(HttpStatus.NOT_FOUND.value());
	}
	
}
