/*Desbilita a verificação das constraints de foreign key*/
set foreign_key_checks = 0;

delete from cidade;
delete from cozinha;
delete from estado;
delete from forma_pagamento;
delete from grupo;
delete from grupo_permissao;
delete from permissao;
delete from produto;
delete from restaurante;
delete from restaurante_forma_pagamento;
delete from usuario;
delete from usuario_grupo;

/*Habilita a verificação das constraints de foreign key*/
set foreign_key_checks = 1;

/*Reset dos sequences*/
alter table cidade auto_increment = 1;
alter table cozinha auto_increment = 1;
alter table estado auto_increment = 1;
alter table forma_pagamento auto_increment = 1;
alter table grupo auto_increment = 1;
alter table grupo_permissao auto_increment = 1;
alter table permissao auto_increment = 1;
alter table produto auto_increment = 1;
alter table restaurante auto_increment = 1;
alter table restaurante_forma_pagamento auto_increment = 1;
alter table usuario auto_increment = 1;
alter table usuario_grupo auto_increment = 1;

/*-------------------------------------------------------------------------------------------------------------*/
/*INSERT - estado*/
INSERT INTO estado (id, nome) VALUES (1, 'São Paulo');
INSERT INTO estado (id, nome) VALUES (2, 'Paraná');
INSERT INTO estado (id, nome) VALUES (3, 'Rio de Janeiro');
INSERT INTO estado (id, nome) VALUES (4, 'Rio Grande do Sul');
INSERT INTO estado (id, nome) VALUES (5, 'Amapá');
INSERT INTO estado (id, nome) VALUES (6, 'Piauí');
/*-------------------------------------------------------------------------------------------------------------*/
/*INSERT - cozinha*/
INSERT INTO cozinha (id,nome) VALUES (1,'Japonesa');
INSERT INTO cozinha (id,nome) VALUES (2,'Brasileira');
INSERT INTO cozinha (id,nome) VALUES (3,'Chinesa');
/*-------------------------------------------------------------------------------------------------------------*/
/*INSERT - forma_pagamento*/
INSERT INTO forma_pagamento (id, descricao) VALUES (1, 'Cartão Crédito');
INSERT INTO forma_pagamento (id, descricao) VALUES (2, 'Cartão Débito');
INSERT INTO forma_pagamento (id, descricao) VALUES (3, 'Vale Refeição');
INSERT INTO forma_pagamento (id, descricao) VALUES (4, 'Pix');
/*-------------------------------------------------------------------------------------------------------------*/
/*INSERT - cidade*/
INSERT INTO cidade (id, nome, estado_id) VALUES (1, 'Santo André', 1);
INSERT INTO cidade (id, nome, estado_id) VALUES (2, 'Curitiba', 2);
INSERT INTO cidade (id, nome, estado_id) VALUES (3, 'Guarulhos', 1);
INSERT INTO cidade (id, nome, estado_id) VALUES (4, 'Porto Alegre', 4);
INSERT INTO cidade (id, nome, estado_id) VALUES (5, 'Petrópolis', 3);
INSERT INTO cidade (id, nome, estado_id) VALUES (6, 'Paranaguá', 2);
INSERT INTO cidade (id, nome, estado_id) VALUES (7, 'Niterói', 3);
/*-------------------------------------------------------------------------------------------------------------*/
/*INSERT - restaurante*/
INSERT INTO restaurante (nome, taxa_frete, cozinha_id, endereco_bairro, endereco_cep, endereco_complemento, endereco_logradouro, endereco_numero, endereco_cidade_id, data_cadastro, data_atualizacao, ativo) VALUES ('Me Lig', 2.5, 3, 'Parque das Nações', '09280-360', 'Apartamento 1', 'Rua Grécia', '5', 1, utc_timestamp, utc_timestamp, true);
INSERT INTO restaurante (nome, taxa_frete, cozinha_id, endereco_bairro, endereco_cep, endereco_complemento, endereco_logradouro, endereco_numero, endereco_cidade_id, data_cadastro, data_atualizacao, ativo) VALUES ('La Marmitex', 5.99, 2, 'Campestre', '09100-001', '', 'Rua Iuguslávia', '540', 2, utc_timestamp, utc_timestamp, true);
INSERT INTO restaurante (nome, taxa_frete, cozinha_id, endereco_bairro, endereco_cep, endereco_complemento, endereco_logradouro, endereco_numero, endereco_cidade_id, data_cadastro, data_atualizacao, ativo) VALUES ('Yamato', 4, 1, 'Jardim Paulista', '12500-150', 'Casa 2', 'Avenida Brasil', 6400, 3, utc_timestamp, utc_timestamp, true);
INSERT INTO restaurante (nome, taxa_frete, cozinha_id, endereco_bairro, endereco_cep, endereco_complemento, endereco_logradouro, endereco_numero, endereco_cidade_id, data_cadastro, data_atualizacao, ativo) VALUES ('Sukiya', 4.98, 1, 'Centro', '08900-123', 'Sala 12', 'Rua Bartira', '32', 7, utc_timestamp, utc_timestamp, true);
INSERT INTO restaurante (nome, taxa_frete, cozinha_id, endereco_bairro, endereco_cep, endereco_complemento, endereco_logradouro, endereco_numero, endereco_cidade_id, data_cadastro, data_atualizacao, ativo) VALUES ('China in Box', 9.41, 3, 'Parque Oratório', '01100-500', '', 'Alameda Costa e Silva', '78', 5, utc_timestamp, utc_timestamp, true);
INSERT INTO restaurante (nome, taxa_frete, cozinha_id, endereco_bairro, endereco_cep, endereco_complemento, endereco_logradouro, endereco_numero, endereco_cidade_id, data_cadastro, data_atualizacao, ativo) VALUES ('Lig Lig', 1.15, 3, 'Brooklyn', '17900-560', 'Apartamento 30', 'Avenida das Nações Unidas', '18002', 1, utc_timestamp, utc_timestamp, true);
INSERT INTO restaurante (nome, taxa_frete, cozinha_id, endereco_bairro, endereco_cep, endereco_complemento, endereco_logradouro, endereco_numero, endereco_cidade_id, data_cadastro, data_atualizacao, ativo) VALUES ('Bom Prato', 0, 2, 'Industrial', '02530-400', 'Sala 1', 'Rua Visconde de Taunay', '152', 4, utc_timestamp, utc_timestamp, true);
INSERT INTO restaurante (nome, taxa_frete, cozinha_id, endereco_bairro, endereco_cep, endereco_complemento, endereco_logradouro, endereco_numero, endereco_cidade_id, data_cadastro, data_atualizacao, ativo) VALUES ('Da Skina', 0, 3, 'Santo Amaro', '04600-120', '', 'Rua das Oliveiras', '123', 3, utc_timestamp, utc_timestamp, true);
/*-------------------------------------------------------------------------------------------------------------*/
/*INSERT - produto*/
INSERT INTO produto (nome, descricao, preco, ativo, restaurante_id) VALUES ('Pastel de pizza', 'Pastel de feira com recheio sabor pizza', 10.90, true, 1);
INSERT INTO produto (nome, descricao, preco, ativo, restaurante_id) VALUES ('Bolinho de queijo', 'Salgado redondo recheado com queijo mussarela', 7.89, true, 1);
INSERT INTO produto (nome, descricao, preco, ativo, restaurante_id) VALUES ('Sashimi de salmão', 'Lombo de salmão em fatias pequenas', 12.70, true, 2);
INSERT INTO produto (nome, descricao, preco, ativo, restaurante_id) VALUES ('Arroz carreteiro', 'Arroz brasileiro com vários ingredientes', 21.10, true, 3);
INSERT INTO produto (nome, descricao, preco, ativo, restaurante_id) VALUES ('Feijão tropeiro', 'Feijão fradinho feito com muito ingredientes e carnes', 17.00, true, 3);
INSERT INTO produto (nome, descricao, preco, ativo, restaurante_id) VALUES ('Yakissoba tradicional M', 'Yakissoba tradicional com carne de frango e bovino. Tamanho M', 30, true, 4);
INSERT INTO produto (nome, descricao, preco, ativo, restaurante_id) VALUES ('Temaki de salmão', 'Temaki com cubos de salmão', 20.99, true, 7);
INSERT INTO produto (nome, descricao, preco, ativo, restaurante_id) VALUES ('Sushi', 'Típico prato japonês feito com arroz, alga marinha e peixe', 8.70, true, 7);
INSERT INTO produto (nome, descricao, preco, ativo, restaurante_id) VALUES ('Feijoada', 'Prato feito com feijão preto e vários cortes suínos', 25.49, true, 8);
INSERT INTO produto (nome, descricao, preco, ativo, restaurante_id) VALUES ('Bife a cavalo', 'Prato de pedreiro composto por muito arroz, feijão carioca, um bife grande e ovo frito', 32.99, true, 8);
/*-------------------------------------------------------------------------------------------------------------*/
/*INSERT - restaurante_forma_pagamento*/
INSERT INTO restaurante_forma_pagamento (restaurante_id, forma_pagamento_id) VALUES (1,1), (1,2), (1,3), (1,4), (2,3), (2,4), (3,1), (4,1), (4,2), (4,4), (5,1), (5,4), (6,2), (8,1), (8,3);
/*-------------------------------------------------------------------------------------------------------------*/
/*INSERT - permissao*/
INSERT INTO permissao (id, nome, descricao) VALUES (1, 'Admin', 'Administrador do sistema');
INSERT INTO permissao (id, nome, descricao) VALUES (2, 'Abrir caixa', 'Pode abrir o caixa');
INSERT INTO permissao (id, nome, descricao) VALUES (3, 'Fechar caixa', 'Pode fechar o caixa');
INSERT INTO permissao (id, nome, descricao) VALUES (4, 'Estornar', 'Pode pode realizar o estorno do valor');
INSERT INTO permissao (id, nome, descricao) VALUES (5, 'Cancelar pedido', 'Pode cancelar pedido');
INSERT INTO permissao (id, nome, descricao) VALUES (6, 'Abrir pedido', 'Pode abrir um novo pedido');
INSERT INTO permissao (id, nome, descricao) VALUES (7, 'Consultar faturamento', 'Pode consultar o faturamento');
/*-------------------------------------------------------------------------------------------------------------*/
/*INSERT - grupo*/
INSERT INTO grupo (id, nome) VALUES (1, 'Administração');
INSERT INTO grupo (id, nome) VALUES (2, 'Caixa');
INSERT INTO grupo (id, nome) VALUES (3, 'Contábil');
/*-------------------------------------------------------------------------------------------------------------*/
/*INSERT - grupo_permissao*/
INSERT INTO grupo_permissao (grupo_id, permissao_id) VALUES (1,1), (2,2), (2,3), (2,4), (2,5), (2,6), (3,7);
/*-------------------------------------------------------------------------------------------------------------*/
/*INSERT - usuario*/
INSERT INTO usuario (id, nome, email, senha, data_cadastro) VALUES (1, 'admin', 'email@email.com', 'PASS123', utc_timestamp);
INSERT INTO usuario (id, nome, email, senha, data_cadastro) VALUES (2, 'caixa001', 'caixa001@email.com', 'CX147', utc_timestamp);
INSERT INTO usuario (id, nome, email, senha, data_cadastro) VALUES (3, 'caixa002', 'caixa002@email.com', 'TP193', utc_timestamp);
INSERT INTO usuario (id, nome, email, senha, data_cadastro) VALUES (4, 'cont001', 'cont001@email.com', 'PDIR195', utc_timestamp);
/*-------------------------------------------------------------------------------------------------------------*/
/*INSERT - usuario_grupo*/
INSERT INTO usuario_grupo (usuario_id, grupo_id) VALUES (1,1), (2,2), (3,2), (4,3);
/*-------------------------------------------------------------------------------------------------------------*/