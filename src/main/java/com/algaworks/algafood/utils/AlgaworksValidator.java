package com.algaworks.algafood.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.SmartValidator;

import com.algaworks.algafood.core.validation.ValidacaoException;

@Component
public class AlgaworksValidator {

	@Autowired
	private SmartValidator validator;
	
	public void validate(Object target, String objectName) {
		BeanPropertyBindingResult bindingResult = new BeanPropertyBindingResult(target, objectName);
		validator.validate(target, bindingResult);
		
		if (bindingResult.hasErrors()) {
			throw new ValidacaoException(bindingResult);
		}
	}
	
}
