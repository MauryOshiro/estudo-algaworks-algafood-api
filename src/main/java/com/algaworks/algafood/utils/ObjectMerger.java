package com.algaworks.algafood.utils;

import java.lang.reflect.Field;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.util.ReflectionUtils;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ObjectMerger {

	public static void mergeObject(Map<String, Object> dataSource, Object objTarget, Class<?> objClazz, 
			HttpServletRequest servletRequest) {
		ServletServerHttpRequest serverHttpRequest = new ServletServerHttpRequest(servletRequest);
		
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, true);
			objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, true);
			
			Object objSource = objectMapper.convertValue(dataSource, objClazz);
			System.out.println(objSource);
			
			dataSource.forEach((property, propertyValue) -> {
				Field field = ReflectionUtils.findField(objClazz, property);
				field.setAccessible(true);
				
				Object newValue = ReflectionUtils.getField(field, objSource);
				
				System.out.println(property + " = " + propertyValue + " = " + newValue);
				
				ReflectionUtils.setField(field, objTarget, newValue);
			});			
		} catch (IllegalArgumentException e) {
			Throwable rootCause = e.getCause();
			throw new HttpMessageNotReadableException(e.getMessage(), rootCause, serverHttpRequest);
		}
	}
}
