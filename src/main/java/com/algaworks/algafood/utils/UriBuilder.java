package com.algaworks.algafood.utils;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UriBuilder {

	@Autowired
	private ServletContext servletContext;
	
	public URI build(String... resources) throws URISyntaxException {
		String uriPath = "";
		for(String r : resources) {
			uriPath = uriPath.concat("/").concat(r);
		}
		
		uriPath = servletContext.getContextPath().concat(uriPath);
		uriPath = uriPath.replaceAll("//", "/");
		
		return new URI(uriPath);
	}
}
