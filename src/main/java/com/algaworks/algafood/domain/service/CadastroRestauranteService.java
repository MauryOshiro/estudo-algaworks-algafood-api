package com.algaworks.algafood.domain.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.algaworks.algafood.domain.exception.EntidadeEmUsoException;
import com.algaworks.algafood.domain.exception.RestauranteNaoEncontradoException;
import com.algaworks.algafood.domain.model.Cozinha;
import com.algaworks.algafood.domain.model.FormaPagamento;
import com.algaworks.algafood.domain.model.Restaurante;
import com.algaworks.algafood.domain.repository.RestauranteRepository;

@Service
public class CadastroRestauranteService {

	private static final String MSG_RESTAURANTE_EM_USO = "Não foi possível excluir restaur ID %d, pois está em uso";

	@Autowired
	private RestauranteRepository restauranteRepository;
	
//	@Autowired
//	private FormaPagamentoRepository formaPagamentoRepository;
	
	@Autowired
	private CadastroCozinhaService cadastroCozinhaService;
	
	@Transactional
	public Restaurante salvar(Restaurante restaurante) {
		Long cozinhaId = restaurante.getCozinha().getId();
		Cozinha cozinha = cadastroCozinhaService.buscarOuFalhar(cozinhaId);
		
		restaurante.setCozinha(cozinha);
		
		List<FormaPagamento> formasPagamentoExistentes = new ArrayList<>();
//		List<FormaPagamento> formasPagamentoOrigem = restaurante.getFormasPagamento();
//		
//		formasPagamentoOrigem.forEach(formaPagamento -> {
//			Long formaPagamentoId = formaPagamento.getId();
//			formaPagamento = formaPagamentoRepository.findById(formaPagamentoId)
//							.orElseThrow(() -> new FormaPagamentoNaoEncontradoException(formaPagamentoId));
//			
//			formasPagamentoExistentes.add(formaPagamento);
//		});
		
		restaurante.setFormasPagamento(formasPagamentoExistentes);
		
		return restauranteRepository.save(restaurante);
	}
	
	@Transactional
	public void excluir(Long restauranteId) {
		try {
			restauranteRepository.deleteById(restauranteId);
			restauranteRepository.flush();
		} catch (EmptyResultDataAccessException e) {
			throw new RestauranteNaoEncontradoException(restauranteId);
		} catch (DataIntegrityViolationException e) {
			String mensagem = String.format(MSG_RESTAURANTE_EM_USO, restauranteId);
			throw new EntidadeEmUsoException(mensagem);
		}
	}
	
	public Restaurante buscaOuFalha(Long restauranteId) {
		return restauranteRepository.findById(restauranteId)
				.orElseThrow(() -> new RestauranteNaoEncontradoException(restauranteId));
	}
	
	@Transactional
	public void ativar(Long restauranteId) {
		Restaurante restauranteAtual = buscaOuFalha(restauranteId);
		restauranteAtual.ativar();
	}
	
	@Transactional
	public void inativar(Long restauranteId) {
		Restaurante restauranteAtual = buscaOuFalha(restauranteId);
		restauranteAtual.inativar();
	}
}
