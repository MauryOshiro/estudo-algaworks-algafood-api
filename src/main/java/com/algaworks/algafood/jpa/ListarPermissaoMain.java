package com.algaworks.algafood.jpa;

import java.util.List;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;

import com.algaworks.algafood.AlgafoodApiApplication;
import com.algaworks.algafood.domain.model.Permissao;
import com.algaworks.algafood.domain.repository.PermissaoRepository;

public class ListarPermissaoMain {

	public static void main(String[] args) {
		ApplicationContext context = new SpringApplicationBuilder(AlgafoodApiApplication.class)
				.web(WebApplicationType.NONE)
				.run(args);
		
		PermissaoRepository repository = context.getBean(PermissaoRepository.class);
		
		List<Permissao> lista = repository.findAll();
		
		if(lista != null) {
			for(Permissao p : lista) {
				System.out.println(p.toString());
			}
		} else {
			System.out.println("Não foi encontrado nenhuma permissão no banco de dados!!!");
		}
	}
}
