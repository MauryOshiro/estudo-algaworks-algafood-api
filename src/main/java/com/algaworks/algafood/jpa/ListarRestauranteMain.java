package com.algaworks.algafood.jpa;

import java.util.List;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;

import com.algaworks.algafood.AlgafoodApiApplication;
import com.algaworks.algafood.domain.model.Restaurante;
import com.algaworks.algafood.domain.repository.RestauranteRepository;

public class ListarRestauranteMain {

	public static void main(String[] args) {
		ApplicationContext context = new SpringApplicationBuilder(AlgafoodApiApplication.class)
				.web(WebApplicationType.NONE)
				.run(args);
		
		RestauranteRepository restauranteRepository = context.getBean(RestauranteRepository.class);
		
		List<Restaurante> listaRestaurante = restauranteRepository.findAll();
		
		if(listaRestaurante != null) {
			for(Restaurante r : listaRestaurante) {
				System.out.println(r.toString());
			}
		} else {
			System.out.println("Não foi encontrado nenhum restaurante no banco de dados!!!");
		}
	}

}
