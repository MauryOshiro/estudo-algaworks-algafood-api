package com.algaworks.algafood.jpa;

import java.util.List;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;

import com.algaworks.algafood.AlgafoodApiApplication;
import com.algaworks.algafood.domain.model.Cidade;
import com.algaworks.algafood.domain.repository.CidadeRepository;

public class ListarCidadeMain {

	public static void main(String[] args) {
		ApplicationContext context = new SpringApplicationBuilder(AlgafoodApiApplication.class)
				.web(WebApplicationType.NONE)
				.run(args);
		
		CidadeRepository repository = context.getBean(CidadeRepository.class);
		
		List<Cidade> lista = repository.findAll();
		
		if(lista != null) {
			for(Cidade c : lista) {
				System.out.println(c.toString());
			}
		} else {
			System.out.println("Não foi encontrada nenhuma cidade no banco de dados!!!");
		}
	}
}
