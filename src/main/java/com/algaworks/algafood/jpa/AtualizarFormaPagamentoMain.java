package com.algaworks.algafood.jpa;

import java.util.Optional;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;

import com.algaworks.algafood.AlgafoodApiApplication;
import com.algaworks.algafood.domain.model.FormaPagamento;
import com.algaworks.algafood.domain.repository.FormaPagamentoRepository;

public class AtualizarFormaPagamentoMain {

	public static void main(String[] args) {
		ApplicationContext context = new SpringApplicationBuilder(AlgafoodApiApplication.class)
				.web(WebApplicationType.NONE)
				.run(args);
		
		FormaPagamentoRepository repository = context.getBean(FormaPagamentoRepository.class);
		Long id = 1L;
		Optional<FormaPagamento> formaPagamentoOpt = repository.findById(id);
		
		if (formaPagamentoOpt.isPresent()) {
			FormaPagamento formaPagamento = formaPagamentoOpt.get();
			System.out.println("Forma de pagamento original");
			System.out.println(formaPagamento.toString());
			System.out.println("=======================================");
			
			formaPagamento.setDescricao("Barra de ouro");
			formaPagamento = repository.save(formaPagamento);
			System.out.println("Forma de pagamento alterada");
			System.out.println(formaPagamento.toString());			
		} else {
			System.out.printf("Não foi encontrado forma de pagamento com ID %d", id);
		}
		
	}
}
