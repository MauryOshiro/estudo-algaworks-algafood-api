package com.algaworks.algafood.jpa;

import java.math.BigDecimal;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;

import com.algaworks.algafood.AlgafoodApiApplication;
import com.algaworks.algafood.domain.model.Restaurante;
import com.algaworks.algafood.domain.repository.RestauranteRepository;

public class InclusaoRestauranteMain {

	public static void main(String[] args) {
		ApplicationContext context = new SpringApplicationBuilder(AlgafoodApiApplication.class)
				.web(WebApplicationType.NONE)
				.run(args);
		
		RestauranteRepository restauranteRepository = context.getBean(RestauranteRepository.class);
		
		BigDecimal taxaFrete = new BigDecimal("0.99");
		
		Restaurante restaurante = new Restaurante();
		restaurante.setNome("Panela Velha");
		restaurante.setTaxaFrete(taxaFrete);
		
		restaurante = restauranteRepository.save(restaurante);
		
		System.out.println("Restaurante salvo com sucesso. ID gerado: " + restaurante.getId());
	}

}
