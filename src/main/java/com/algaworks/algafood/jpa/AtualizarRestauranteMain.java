package com.algaworks.algafood.jpa;

import java.math.BigDecimal;
import java.util.Optional;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;

import com.algaworks.algafood.AlgafoodApiApplication;
import com.algaworks.algafood.domain.model.Restaurante;
import com.algaworks.algafood.domain.repository.RestauranteRepository;

public class AtualizarRestauranteMain {

	public static void main(String[] args) {
		ApplicationContext context = new SpringApplicationBuilder(AlgafoodApiApplication.class)
				.web(WebApplicationType.NONE)
				.run(args);
		
		RestauranteRepository restauranteRepository = context.getBean(RestauranteRepository.class);
		
		Long id = 1L;
		Optional<Restaurante> restauranteOpt = restauranteRepository.findById(id);
		
		if(restauranteOpt.isPresent()) {
			Restaurante restaurante = restauranteOpt.get();
			System.out.println("Restaurante encontrado");
			System.out.println("ID: " + restaurante.getId());
			System.out.println("Nome: " + restaurante.getNome());
			System.out.println("Taxa frete: " + restaurante.getTaxaFrete());
			System.out.println("===============================");
			
			restaurante.setNome("Sukiya");
			BigDecimal taxaFrete = new BigDecimal("1.45");
			restaurante.setTaxaFrete(taxaFrete);
			restaurante = restauranteRepository.save(restaurante);
			
			System.out.println("Restaurante atualizado");
			System.out.println("ID: " + restaurante.getId());
			System.out.println("Nome: " + restaurante.getNome());
			System.out.println("Taxa frete: " + restaurante.getTaxaFrete());
			System.out.println("===============================");
		} else {
			System.out.println("Restaurante não encontrado pelo ID " + id);
		}
	}
}
