package com.algaworks.algafood.jpa;

import java.util.List;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;

import com.algaworks.algafood.AlgafoodApiApplication;
import com.algaworks.algafood.domain.model.Estado;
import com.algaworks.algafood.domain.repository.EstadoRepository;

public class ListarEstadoMain {

	public static void main(String[] args) {
		ApplicationContext context = new SpringApplicationBuilder(AlgafoodApiApplication.class)
				.web(WebApplicationType.NONE)
				.run(args);
		
		EstadoRepository repository = context.getBean(EstadoRepository.class);
		List<Estado> lista = repository.findAll();
		
		if(lista != null) {
			for(Estado e : lista) {
				System.out.println(e.toString());
			}
		} else {
			System.out.println("Não foi encontrado nenhum estado no banco de dados!!!");
		}
	}
}
