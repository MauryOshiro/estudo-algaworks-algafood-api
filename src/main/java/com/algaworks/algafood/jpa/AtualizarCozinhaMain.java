package com.algaworks.algafood.jpa;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;

import com.algaworks.algafood.AlgafoodApiApplication;
import com.algaworks.algafood.domain.exception.CozinhaNaoEncontradaException;
import com.algaworks.algafood.domain.model.Cozinha;
import com.algaworks.algafood.domain.repository.CozinhaRepository;

public class AtualizarCozinhaMain {

	public static void main(String[] args) {
		ApplicationContext context = new SpringApplicationBuilder(AlgafoodApiApplication.class)
				.web(WebApplicationType.SERVLET)
				.run(args);
		
		CozinhaRepository cozinhaRepository = context.getBean(CozinhaRepository.class);
		
		Cozinha cozinha = new Cozinha();
		cozinha = cozinhaRepository.findById(1L)
				.orElseThrow(() -> new CozinhaNaoEncontradaException("Cozinha não encontrada"));
		
		if(cozinha != null) {
			cozinha.setNome("Francesa");
			cozinhaRepository.save(cozinha);
			System.out.println("Cozinha atualizada com sucesso!");
		} else {
			System.out.println("ID não encontrado na base de dados!!!");
		}
	}

}
