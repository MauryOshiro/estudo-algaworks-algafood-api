package com.algaworks.algafood.jpa;

import java.util.Optional;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;

import com.algaworks.algafood.AlgafoodApiApplication;
import com.algaworks.algafood.domain.model.Estado;
import com.algaworks.algafood.domain.repository.EstadoRepository;

public class BuscarEstadoMain {

	public static void main(String[] args) {
		ApplicationContext context = new SpringApplicationBuilder(AlgafoodApiApplication.class)
				.web(WebApplicationType.NONE)
				.run(args);
		
		EstadoRepository repository = context.getBean(EstadoRepository.class);
		
		Long id = 2L;
		Optional<Estado> estadoOpt = repository.findById(id);
		
		if(estadoOpt.isPresent())
			System.out.println(estadoOpt.get().toString());
		else
			System.out.println("Não foi encontrado estado pelo ID " + id);
	}
}
