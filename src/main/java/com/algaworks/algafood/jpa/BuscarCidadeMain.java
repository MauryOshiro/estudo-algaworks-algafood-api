package com.algaworks.algafood.jpa;

import java.util.Optional;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;

import com.algaworks.algafood.AlgafoodApiApplication;
import com.algaworks.algafood.domain.model.Cidade;
import com.algaworks.algafood.domain.repository.CidadeRepository;

public class BuscarCidadeMain {

	public static void main(String[] args) {
		ApplicationContext context = new SpringApplicationBuilder(AlgafoodApiApplication.class)
				.web(WebApplicationType.NONE)
				.run(args);
		
		CidadeRepository repository = context.getBean(CidadeRepository.class);
		
		Long id = 3L;
		Optional<Cidade> cidadeOpt = repository.findById(id);
		
		if(cidadeOpt.isPresent())
			System.out.println(cidadeOpt.get().toString());
		else
			System.out.println("Cidade não encontrada pelo ID " + id);
	}
}
