package com.algaworks.algafood.jpa;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;

import com.algaworks.algafood.AlgafoodApiApplication;
import com.algaworks.algafood.domain.model.Cozinha;
import com.algaworks.algafood.domain.repository.CozinhaRepository;

public class InclusaoCozinhaMain {

	public static void main(String[] args) {
		ApplicationContext context = new SpringApplicationBuilder(AlgafoodApiApplication.class)
				.web(WebApplicationType.NONE)
				.run(args);
		
		CozinhaRepository cozinhaRepository = context.getBean(CozinhaRepository.class);
		Cozinha c1 = new Cozinha();
		c1.setNome("Tailandesa");
		
		Cozinha c2 = new Cozinha();
		c2.setNome("Mexicana");
		
		System.out.println("ID de c1 antes de persistir: " + c1.getId());
		System.out.println("ID de c2 antes de persistir: " + c2.getId());
		
		c1 = cozinhaRepository.save(c1);
		c2 = cozinhaRepository.save(c2);
		
		System.out.println("ID de c1 depois de persistir: " + c1.getId());
		System.out.println("ID de c2 depois de persistir: " + c2.getId());
		
	}

}
