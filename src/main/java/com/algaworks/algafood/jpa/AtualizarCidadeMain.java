package com.algaworks.algafood.jpa;

import java.util.Optional;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;

import com.algaworks.algafood.AlgafoodApiApplication;
import com.algaworks.algafood.domain.model.Cidade;
import com.algaworks.algafood.domain.model.Estado;
import com.algaworks.algafood.domain.repository.CidadeRepository;

public class AtualizarCidadeMain {

	public static void main(String[] args) {

		ApplicationContext context = new SpringApplicationBuilder(AlgafoodApiApplication.class)
				.web(WebApplicationType.SERVLET).run(args);

		CidadeRepository repository = context.getBean(CidadeRepository.class);

		Optional<Cidade> cidadeOpt = repository.findById(2L);

		if (cidadeOpt != null) {
			Cidade cidade = cidadeOpt.get();
			System.out.println("Cidade original");
			System.out.println(cidade.toString());

			Estado estado = new Estado();
			estado.setId(3L);
			cidade.setNome("New York");
			cidade.setEstado(estado);
			cidade = repository.save(cidade);

			System.out.println("Cidade atualizada");
			System.out.println(cidade.toString());
		} else {
			System.out.println("Não foi encontrada cidade pelo ID!!!");
		}

	}
}
