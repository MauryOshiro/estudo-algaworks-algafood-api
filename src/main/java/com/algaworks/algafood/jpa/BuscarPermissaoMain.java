package com.algaworks.algafood.jpa;

import java.util.Optional;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;

import com.algaworks.algafood.AlgafoodApiApplication;
import com.algaworks.algafood.domain.model.Permissao;
import com.algaworks.algafood.domain.repository.PermissaoRepository;

public class BuscarPermissaoMain {

	public static void main(String[] args) {
		ApplicationContext context = new SpringApplicationBuilder(AlgafoodApiApplication.class)
				.web(WebApplicationType.NONE)
				.run(args);
		
		PermissaoRepository repository = context.getBean(PermissaoRepository.class);
		
		Long id = 1L;
		Optional<Permissao> permissaoOpt = repository.findById(id);
		
		if(permissaoOpt.isPresent())
			System.out.println(permissaoOpt.get().toString());
		else
			System.out.println("Permissão não encontrada pelo ID " + id);
	}
}
