package com.algaworks.algafood.jpa;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;

import com.algaworks.algafood.AlgafoodApiApplication;
import com.algaworks.algafood.domain.model.Permissao;
import com.algaworks.algafood.domain.repository.PermissaoRepository;

public class InclusaoPermissaoMain {

	public static void main(String[] args) {
		ApplicationContext context = new SpringApplicationBuilder(AlgafoodApiApplication.class)
				.web(WebApplicationType.NONE)
				.run(args);
		
		PermissaoRepository repository = context.getBean(PermissaoRepository.class);
		
		Permissao permissao = new Permissao();
		permissao.setNome("Ajudante");
		permissao.setDescricao("Permissão de ajudante. Somente consulta e acesso restrito a certas features.");
		
		permissao = repository.save(permissao);
		System.out.println("Permissão salva");
		System.out.println(permissao.toString());
	}
}
