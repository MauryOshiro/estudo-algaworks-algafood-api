package com.algaworks.algafood.jpa;

import java.util.Optional;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;

import com.algaworks.algafood.AlgafoodApiApplication;
import com.algaworks.algafood.domain.model.Permissao;
import com.algaworks.algafood.domain.repository.PermissaoRepository;

public class AtualizarPermissaoMain {

	public static void main(String[] args) {
		ApplicationContext context = new SpringApplicationBuilder(AlgafoodApiApplication.class)
				.web(WebApplicationType.NONE)
				.run(args);
		
		PermissaoRepository repository = context.getBean(PermissaoRepository.class);
		Long id = 1L;
		Optional<Permissao> permissaoOpt = repository.findById(id);
		
		if (permissaoOpt.isPresent()) {
			Permissao permissao = permissaoOpt.get(); 
			System.out.println("Permissão original");
			System.out.println(permissao.toString());
			
			permissao.setNome("Master");
			permissao.setDescricao("Mestre do sistema. Ele faz magia!");
			
			permissao = repository.save(permissao);
			System.out.println("Permissão alterada");
			System.out.println(permissao.toString());			
		} else {
			System.out.printf("Não foi encontrado permissão com ID %d", id);
		}
	}
}
