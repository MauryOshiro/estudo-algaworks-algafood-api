package com.algaworks.algafood.jpa;

import java.util.Optional;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;

import com.algaworks.algafood.AlgafoodApiApplication;
import com.algaworks.algafood.domain.model.FormaPagamento;
import com.algaworks.algafood.domain.repository.FormaPagamentoRepository;

public class BuscarFormaPagamentoMain {

	public static void main(String[] args) {
		ApplicationContext context = new SpringApplicationBuilder(AlgafoodApiApplication.class)
				.web(WebApplicationType.NONE)
				.run(args);
		
		FormaPagamentoRepository repository = context.getBean(FormaPagamentoRepository.class);
		
		Long id = 4L;
		Optional<FormaPagamento> formaPagamentoOpt = repository.findById(id);
		
		if(formaPagamentoOpt.isPresent())
			System.out.println(formaPagamentoOpt.toString());
		else
			System.out.println("Forma de pagamento não encontrado pelo ID " + id);
	}
}
