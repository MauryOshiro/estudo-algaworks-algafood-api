package com.algaworks.algafood.jpa;

import java.util.Optional;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;

import com.algaworks.algafood.AlgafoodApiApplication;
import com.algaworks.algafood.domain.model.Estado;
import com.algaworks.algafood.domain.repository.EstadoRepository;

public class AtualizarEstadoMain {

	public static void main(String[] args) {
		ApplicationContext context = new SpringApplicationBuilder(AlgafoodApiApplication.class)
				.web(WebApplicationType.NONE)
				.run(args);
		
		EstadoRepository repository = context.getBean(EstadoRepository.class);
		
		Optional<Estado> estadoOpt = repository.findById(1L);
		
		if(estadoOpt.isPresent()) {
			Estado estado = estadoOpt.get();
			System.out.println("Estado original");
			System.out.println(estado.toString());
			
			estado.setNome("Santa Catarina");
			estado = repository.save(estado);
			
			System.out.println("Estado atualizado");
			System.out.println(estado.toString());
		} else {
			System.out.println("Estado não encontrado pelo ID");
		}
	}
}
