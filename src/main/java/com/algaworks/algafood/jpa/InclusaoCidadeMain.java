package com.algaworks.algafood.jpa;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;

import com.algaworks.algafood.AlgafoodApiApplication;
import com.algaworks.algafood.domain.model.Cidade;
import com.algaworks.algafood.domain.model.Estado;
import com.algaworks.algafood.domain.repository.CidadeRepository;

public class InclusaoCidadeMain {

	public static void main(String[] args) {
		ApplicationContext context = new SpringApplicationBuilder(AlgafoodApiApplication.class)
				.web(WebApplicationType.SERVLET)
				.run(args);
		
		CidadeRepository repository = context.getBean(CidadeRepository.class);
		
		Estado e1 = new Estado();
		e1.setId(1L);
		
		Cidade c1 = new Cidade();
		c1.setNome("São Caetano do Sul");
		c1.setEstado(e1);
		
		c1 = repository.save(c1);
		System.out.println("Cidade salva");
		System.out.println(c1.toString());
		
		Estado e2 = new Estado();
		e2.setId(4L);
		
		Cidade c2 = new Cidade();
		c2.setNome("Águas Claras");
		c2.setEstado(e2);
		
		c2 = repository.save(c2);
		System.out.println("Cidade salva");
		System.out.println(c2.toString());
	}
}
