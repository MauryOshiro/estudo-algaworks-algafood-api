package com.algaworks.algafood.jpa;

import java.util.Optional;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;

import com.algaworks.algafood.AlgafoodApiApplication;
import com.algaworks.algafood.domain.model.Restaurante;
import com.algaworks.algafood.domain.repository.RestauranteRepository;

public class BuscarRestauranteMain {

	public static void main(String[] args) {
		ApplicationContext context = new SpringApplicationBuilder(AlgafoodApiApplication.class)
				.web(WebApplicationType.NONE)
				.run(args);
		
		RestauranteRepository restauranteRepository = context.getBean(RestauranteRepository.class);
		
		Restaurante r = new Restaurante();
		r.setId(3L);
		
		Optional<Restaurante> restauranteOpt = restauranteRepository.findById(r.getId());
		
		if(restauranteOpt.isPresent()) {
			Restaurante restaurante = restauranteOpt.get();
			System.out.println("Restaurante encontrado pelo ID " + r.getId() + ". Nome: " 
					+ restaurante.getNome() + " | Taxa frete: " + restaurante.getTaxaFrete());			
		} else {
			System.out.println("Não foi encontrado restaurante pelo ID " + r.getId());			
		}
	}
}
