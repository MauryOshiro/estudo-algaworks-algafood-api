package com.algaworks.algafood.api.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.algaworks.algafood.api.assembler.RestauranteInputDisassembler;
import com.algaworks.algafood.api.assembler.RestauranteModelAssembler;
import com.algaworks.algafood.api.model.RestauranteModel;
import com.algaworks.algafood.api.model.input.RestauranteInput;
import com.algaworks.algafood.domain.exception.CozinhaNaoEncontradaException;
import com.algaworks.algafood.domain.exception.FormaPagamentoNaoEncontradaException;
import com.algaworks.algafood.domain.exception.NegocioException;
import com.algaworks.algafood.domain.model.Restaurante;
import com.algaworks.algafood.domain.repository.RestauranteRepository;
import com.algaworks.algafood.domain.service.CadastroRestauranteService;
import com.algaworks.algafood.utils.UriBuilder;

@RestController
@RequestMapping(value = RestauranteController.ENDPOINT_URI)
public class RestauranteController {

	public static final String ENDPOINT_URI = "/v1/restaurantes";

	@Autowired
	private CadastroRestauranteService restauranteService;

	@Autowired
	private RestauranteRepository restauranteRepository;

	@Autowired
	private UriBuilder uriBuilder;
	
//	@Autowired
//	private AlgaworksValidator validator;

	@Autowired
	private RestauranteModelAssembler restauranteModelAssembler;
	
	@Autowired
	private RestauranteInputDisassembler restauranteInputDisassembler;
	
	@GetMapping
	public ResponseEntity<List<RestauranteModel>> listar() {
		List<Restaurante> listaRestaurante = restauranteRepository.findAll();
		return ResponseEntity.ok(restauranteModelAssembler.toCollectionModel(listaRestaurante));
	}

	@ResponseStatus(value = HttpStatus.OK)
	@GetMapping("/{restauranteId}")
	public RestauranteModel buscar(@PathVariable Long restauranteId) {
		Restaurante restaurante = restauranteService.buscaOuFalha(restauranteId);
		
		return restauranteModelAssembler.toModel(restaurante);
	}

	@PostMapping
	public ResponseEntity<RestauranteModel> salvar(@RequestBody @Valid RestauranteInput restauranteInput) 
					throws URISyntaxException {
		try {
			Restaurante restaurante = restauranteInputDisassembler.toDomainObject(restauranteInput);
			restaurante = restauranteService.salvar(restaurante);
			URI uri = uriBuilder.build(ENDPOINT_URI, restaurante.getId().toString());
			
			return ResponseEntity.created(uri).body(restauranteModelAssembler.toModel(restaurante));			
		} catch (CozinhaNaoEncontradaException e) {
			throw new NegocioException(e.getMessage(), e);
		} catch (FormaPagamentoNaoEncontradaException e) {
			throw new NegocioException(e.getMessage(), e);
		}
	}

	@ResponseStatus(value = HttpStatus.OK)
	@PutMapping("/{restauranteId}")
	public RestauranteModel atualizar(@PathVariable Long restauranteId, @RequestBody @Valid RestauranteInput restauranteInput) {
		Restaurante restauranteAtual = restauranteService.buscaOuFalha(restauranteId);
		
		restauranteInputDisassembler.copyToDomainObject(restauranteInput, restauranteAtual);
		
		try {
			return restauranteModelAssembler.toModel(restauranteService.salvar(restauranteAtual));			
		} catch (CozinhaNaoEncontradaException e) {
			throw new NegocioException(e.getMessage(), e);
		} catch (FormaPagamentoNaoEncontradaException e) {
			throw new NegocioException(e.getMessage(), e);
		}
	}

	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	@DeleteMapping("/{restauranteId}")
	public void remover(@PathVariable Long restauranteId) {
		restauranteService.excluir(restauranteId);
	}
	
//	@ResponseStatus(value = HttpStatus.OK)
//	@PatchMapping("/{restauranteId}")
//	public RestauranteModel atualizarParcial(@PathVariable Long restauranteId, @RequestBody Map<String, Object> campos, 
//			HttpServletRequest servletRequest) {
//		Restaurante restauranteAtual = restauranteService.buscaOuFalha(restauranteId);
//		ObjectMerger.mergeObject(campos, restauranteAtual, Restaurante.class, servletRequest);
//		
//		//validate(restauranteAtual, "restaurante");
//		
//		validator.validate(restauranteAtual, "restaurante");
//		
//		return atualizar(restauranteId, restauranteAtual);
//	}
	
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	@PutMapping("/{restauranteId}/ativo")
	public void ativar(@PathVariable Long restauranteId) {
		restauranteService.ativar(restauranteId);
	}	
	
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	@DeleteMapping("/{restauranteId}/ativo")
	public void inativar(@PathVariable Long restauranteId) {
		restauranteService.inativar(restauranteId);
	}
}
