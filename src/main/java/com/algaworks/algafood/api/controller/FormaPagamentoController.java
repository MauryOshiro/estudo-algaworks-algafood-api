package com.algaworks.algafood.api.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.algaworks.algafood.api.assembler.FormaPagamentoInputDisassembler;
import com.algaworks.algafood.api.assembler.FormaPagamentoModelAssembler;
import com.algaworks.algafood.api.model.FormaPagamentoModel;
import com.algaworks.algafood.api.model.input.FormaPagamentoInput;
import com.algaworks.algafood.domain.model.FormaPagamento;
import com.algaworks.algafood.domain.service.CadastroFormaPagamentoService;
import com.algaworks.algafood.utils.UriBuilder;

@RestController
@RequestMapping(value = FormaPagamentoController.ENDPOINT_URI)
public class FormaPagamentoController {

	public static final String ENDPOINT_URI = "/v1/formas-pagamento";
	
	@Autowired
	private FormaPagamentoInputDisassembler formaPagamentoInputDisassembler;
	
	@Autowired
	private FormaPagamentoModelAssembler formaPagamentoModelAssembler;
	
	@Autowired
	private CadastroFormaPagamentoService cadastroFormaPagamentoService;
	
	@Autowired
	private UriBuilder uriBuilder;
	
	@PostMapping
	public ResponseEntity<FormaPagamentoModel> salvar(@RequestBody @Valid FormaPagamentoInput formaPagamentoInput) throws URISyntaxException {
		FormaPagamento formaPagamento = this.formaPagamentoInputDisassembler.toDomainModel(formaPagamentoInput);
		
		formaPagamento = this.cadastroFormaPagamentoService.salvar(formaPagamento);
		FormaPagamentoModel formaPagamentoModel = this.formaPagamentoModelAssembler.toModel(formaPagamento);
		URI uri = this.uriBuilder.build(ENDPOINT_URI, formaPagamentoModel.getId().toString());
		
		return ResponseEntity.created(uri).body(formaPagamentoModel);
	}
	
	@ResponseStatus(code = HttpStatus.OK)
	@GetMapping
	public List<FormaPagamentoModel> listar() {
		List<FormaPagamento> listaFormasPagamento = this.cadastroFormaPagamentoService.listar();
		
		return this.formaPagamentoModelAssembler.toCollectionModel(listaFormasPagamento);
	}
	
	@ResponseStatus(code = HttpStatus.OK)
	@GetMapping("/{formaPagamentoId}")
	public FormaPagamentoModel buscar(@PathVariable Long formaPagamentoId) {
		FormaPagamento formaPagamento = this.cadastroFormaPagamentoService.buscarOuFalhar(formaPagamentoId);
		
		return this.formaPagamentoModelAssembler.toModel(formaPagamento);
	}
	
	@ResponseStatus(code = HttpStatus.OK)
	@PutMapping("/{formaPagamentoId}")
	public FormaPagamentoModel atualizar(@PathVariable Long formaPagamentoId, 
			@RequestBody @Valid FormaPagamentoInput input) {
		FormaPagamento formaPagamentoAtual = this.cadastroFormaPagamentoService.buscarOuFalhar(formaPagamentoId);
		this.formaPagamentoInputDisassembler.copyToDomainObject(input, formaPagamentoAtual);
		formaPagamentoAtual = this.cadastroFormaPagamentoService.salvar(formaPagamentoAtual);
		
		return this.formaPagamentoModelAssembler.toModel(formaPagamentoAtual);
	}
	
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	@DeleteMapping("/{formaPagamentoId}")
	public void excluir(@PathVariable Long formaPagamentoId) {
		this.cadastroFormaPagamentoService.excluir(formaPagamentoId);
	}
	
}
