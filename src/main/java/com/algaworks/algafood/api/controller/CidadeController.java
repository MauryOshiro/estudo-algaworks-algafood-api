package com.algaworks.algafood.api.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.algaworks.algafood.api.assembler.CidadeInputDisassembler;
import com.algaworks.algafood.api.assembler.CidadeModelAssembler;
import com.algaworks.algafood.api.model.CidadeModel;
import com.algaworks.algafood.api.model.input.CidadeInput;
import com.algaworks.algafood.domain.exception.EstadoNaoEncontradoException;
import com.algaworks.algafood.domain.exception.NegocioException;
import com.algaworks.algafood.domain.model.Cidade;
import com.algaworks.algafood.domain.repository.CidadeRepository;
import com.algaworks.algafood.domain.service.CadastroCidadeService;
import com.algaworks.algafood.utils.UriBuilder;

@RestController
@RequestMapping(value = CidadeController.ENDPOINT)
public class CidadeController {

	public static final String ENDPOINT = "/v1/cidades";
	
	@Autowired
	private CidadeRepository cidadeRepository;
	
	@Autowired
	private CadastroCidadeService cidadeService;
	
	@Autowired
	private UriBuilder uriBuilder;
	
	@Autowired
	private CidadeModelAssembler cidadeModelAssembler;
	
	@Autowired
	private CidadeInputDisassembler cidadeInputDisassembler;
	
	@GetMapping
	public ResponseEntity<List<CidadeModel>> listar() {
		List<Cidade> lista = cidadeRepository.findAll();
		
		return ResponseEntity.ok(cidadeModelAssembler.toCollectionModel(lista));
	}
	
	@ResponseStatus(value = HttpStatus.OK)
	@GetMapping("/{cidadeId}")
	public CidadeModel buscar(@PathVariable Long cidadeId) {
		return cidadeModelAssembler.toModel(cidadeService.buscaOuFalha(cidadeId));
	}
	
	@PostMapping
	public ResponseEntity<CidadeModel> cadastrar(@RequestBody @Valid CidadeInput cidadeInput) throws URISyntaxException {
		try {
			Cidade cidade = cidadeInputDisassembler.toDomainModel(cidadeInput);
			cidade = cidadeService.salvar(cidade);
			URI uri = uriBuilder.build(ENDPOINT, cidade.getId().toString());
			
			CidadeModel cidadeModel = cidadeModelAssembler.toModel(cidade);
			
			return ResponseEntity.created(uri).body(cidadeModel);			
		} catch (EstadoNaoEncontradoException e) {
			throw new NegocioException(e.getMessage(), e);
		}
	}
	
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	@DeleteMapping("/{cidadeId}")
	public void excluir(@PathVariable Long cidadeId) {
		cidadeService.excluir(cidadeId);
	}
	
	@ResponseStatus(value = HttpStatus.OK)
	@PutMapping("/{cidadeId}")
	public CidadeModel atualizar(@PathVariable Long cidadeId, @RequestBody @Valid CidadeInput cidadeInput) {
		try {
			Cidade cidadeAtual = cidadeService.buscaOuFalha(cidadeId);
			cidadeInputDisassembler.copyToDomainModel(cidadeInput, cidadeAtual);
			
			return cidadeModelAssembler.toModel(cidadeService.salvar(cidadeAtual));
		} catch (EstadoNaoEncontradoException e) {
			throw new NegocioException(e.getMessage(), e);
		}
	}
}
