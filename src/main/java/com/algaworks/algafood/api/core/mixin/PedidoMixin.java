package com.algaworks.algafood.api.core.mixin;

import com.algaworks.algafood.domain.model.FormaPagamento;
import com.algaworks.algafood.domain.model.Restaurante;
import com.algaworks.algafood.domain.model.Usuario;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

public class PedidoMixin {

	@JsonIgnoreProperties("hibernateLazyInitializer")
	private FormaPagamento formaPagamento;
	
	@JsonIgnoreProperties("hibernateLazyInitializer")
	private Restaurante restaurante;

	@JsonIgnoreProperties("hibernateLazyInitializer")
	private Usuario cliente;
	
}
