package com.algaworks.algafood.api.core.mixin;

import com.algaworks.algafood.domain.model.Pedido;
import com.algaworks.algafood.domain.model.Produto;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

public class ItemPedidoMixin {
	
	@JsonIgnoreProperties("hibernateLazyInitializer")
	private Produto produto;
	
	@JsonIgnoreProperties("hibernateLazyInitializer")
	private Pedido pedido;

}
