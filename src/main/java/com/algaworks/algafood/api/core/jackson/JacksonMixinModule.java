package com.algaworks.algafood.api.core.jackson;

import org.springframework.stereotype.Component;

import com.algaworks.algafood.api.core.mixin.CidadeMixin;
import com.algaworks.algafood.api.core.mixin.CozinhaMixin;
import com.algaworks.algafood.api.core.mixin.EnderecoMixin;
import com.algaworks.algafood.api.core.mixin.ItemPedidoMixin;
import com.algaworks.algafood.api.core.mixin.PedidoMixin;
import com.algaworks.algafood.api.core.mixin.ProdutoMixin;
import com.algaworks.algafood.domain.model.Cidade;
import com.algaworks.algafood.domain.model.Cozinha;
import com.algaworks.algafood.domain.model.Endereco;
import com.algaworks.algafood.domain.model.ItemPedido;
import com.algaworks.algafood.domain.model.Pedido;
import com.algaworks.algafood.domain.model.Produto;
import com.fasterxml.jackson.databind.module.SimpleModule;

@Component
public class JacksonMixinModule extends SimpleModule {

	private static final long serialVersionUID = 1L;

	public JacksonMixinModule() {
		setMixInAnnotation(Cidade.class, CidadeMixin.class);
		setMixInAnnotation(Cozinha.class, CozinhaMixin.class);
		setMixInAnnotation(Endereco.class, EnderecoMixin.class);
		setMixInAnnotation(ItemPedido.class, ItemPedidoMixin.class);
		setMixInAnnotation(Pedido.class, PedidoMixin.class);
		setMixInAnnotation(Produto.class, ProdutoMixin.class);
	}
	
}
