package com.algaworks.algafood.api.exceptionhandler;

import lombok.Getter;

@Getter
public enum ProblemType {

	RECURSO_NAO_ENCONTRADO("Recurso não encontrado", "/recurso-nao-encontrado"),
	ENTIDADE_EM_USO("Entidade em uso","/entidade-em-uso"),
	ERRO_NEGOCIO("Violação de regra de negócio","/erro-negocio"),
	MENSAGEM_INCOMPREENSIVEL("Mensagem incompreensível", "/mensagem-incompreensivel"),
	PROPRIEDADE_INEXISTENTE("Propriedade inexistente", "/propriedade-inexistente"),
	PARAMETRO_INVALIDO("Parâmetro inválido", "/parametro-invalido"),
	ERRO_DE_SISTEMA("Errp de sistema", "/erro-de-sistema"),
	DADOS_INVALIDOS("Dados inválidos", "/dados-invalidos");

	private String title;
	private String uri;

	ProblemType(String title, String path) {
		this.title = title;
		this.uri = "https://algafood.com.br".concat(path);
	}

}
